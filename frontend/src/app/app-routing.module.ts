import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',

    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule),
    //  canLoad: [ AuthGuardServiceGuard ]
  },
  {
    path: 'conversacion',

    loadChildren: () => import('./modules/conversacion/conversacion.module').then(m => m.ConversacionModule),
    //  canLoad: [ AuthGuardServiceGuard ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
