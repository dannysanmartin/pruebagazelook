import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Socket } from 'ngx-socket-io';

@Injectable({
  providedIn: 'root'
})
export class ChatServiceService {
  urlBack = environment.urlDB;
  constructor( private httpClient: HttpClient, private socket: Socket ) { }
  
  createUser(dataLogin: any): any {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(`${this.urlBack}users/createUser`, dataLogin, {headers});
  }
  getUser(): any{
    return this.httpClient.get(`${this.urlBack}users/listUser`);
  }

  getRols(): any{
    return this.httpClient.get(`${this.urlBack}users/listRols`);
  }

  createGroup(dataLogin: any): any {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(`${this.urlBack}groups/createGroup`, dataLogin, {headers});
  }

  getGroups(): any{
    return this.httpClient.get(`${this.urlBack}groups/listGroups`);
  }


  createService(dataLogin: any): any {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(`${this.urlBack}users/createUser`, dataLogin, {headers});
  }

  loginService(dataLogin: any): any {
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    return this.httpClient.post(`${this.urlBack}auth/login`, dataLogin, {headers});
  }
  getProfileService(token: string): any {
    const headers = new HttpHeaders({'Authorization': `Bearer ${token}`});
    return this.httpClient.get(`${this.urlBack}auth/profile`, {headers});
  }

  sendChat(message): any{
    this.socket.emit('chat', message);
  }

  sendUserID(message): any{
    this.socket.emit('userID', message);
  }

  receiveUserID(): any{
    return this.socket.fromEvent('userID');
  }


  receiveChat(): any{
    return this.socket.fromEvent('chat');
  }

  getUsers(): any{
    return this.socket.fromEvent('users');
  }
}
