import { json } from 'body-parser';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChatServiceService } from 'src/app/shared/chat-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  roles = [];

  formLogin: FormGroup = this.formBuilder.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
  });

  formRegister: FormGroup = this.formBuilder.group({
    nombres: ['', [Validators.required]],
    apellidos: ['', [Validators.required]],
    email: ['', [Validators.required]],
    password: ['', [Validators.required]],
    rol: ['', [Validators.required]]
  });

  constructor(private formBuilder: FormBuilder, private serviceChat: ChatServiceService, private router: Router) { }

  ngOnInit(): void {
    this.getRoles();
  }

  loginUser(): any {
    console.log(this.formLogin.value);
    this.serviceChat.loginService(this.formLogin.value).subscribe(data => {
      console.log(data);
      this.serviceChat.getProfileService(data.access_token).subscribe(dataUser => {
        console.log(dataUser);
        localStorage.setItem('loginUser', JSON.stringify(dataUser.user));
        this.router.navigate(['/conversacion']);
      });
    });

  }
  registerUser(): any {
    console.log(this.formRegister.value);
    this.serviceChat.createUser(this.formRegister.value).subscribe(dataCreate => {
      console.log(dataCreate);
      alert('creado con exito');
      this.formRegister.reset();
    });

  }
  getRoles(): any {
    this.serviceChat.getRols().subscribe(dataUser => {
      this.roles = dataUser;
    });
  }
 

}
