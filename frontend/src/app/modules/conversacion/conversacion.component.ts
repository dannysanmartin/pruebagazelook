import { Component, OnInit } from '@angular/core';
import { ChatServiceService } from 'src/app/shared/chat-service.service';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AnonymousSubject } from 'rxjs/internal/Subject';
import * as _ from 'lodash';

@Component({
  selector: 'app-conversacion',
  templateUrl: './conversacion.component.html',
  styleUrls: ['./conversacion.component.scss']
})
export class ConversacionComponent implements OnInit {
  public users = 0;
  public message = '';
  public messages: string[] = [];
  messageSave = [];
  usersList = [];
  groupList = [];
  userGroups = [];
  nameUser: any;
  selectedUser: any;
  selectedUserData: any;

  formCreateGroup: FormGroup = this.formBuilder.group({
    nombreGrupo: ['', [Validators.required]],
  });

  mensajesChat: FormGroup = this.formBuilder.group({
    mensaje: ['', [Validators.required]],
  });

  constructor(private formBuilder: FormBuilder, private chatService: ChatServiceService) { }

  ngOnInit(): void {
    this.getUsers();
    this.getGroups();
    // this.chatService.receiveChat().subscribe((message: string) => {
    //   this.messages.push(message);
    // });
    this.chatService.receiveChat().subscribe((message: string) => {
      this.messageSave.push(message);
    });

    this.chatService.receiveUserID().subscribe((idUser: string) => {
      this.messages.push(idUser);
    });

    this.chatService.getUsers().subscribe((users: number) => {
      this.users = users;
    });
    this.nameUser = JSON.parse(localStorage.getItem('loginUser'));
  }
  addChat(): any {
    console.log(this.mensajesChat.controls.mensaje.value);
    const msj = {
      userId: this.nameUser._id,
      nameUser: this.nameUser.nombres,
      mensaje: this.mensajesChat.controls.mensaje.value
    };
    this.messageSave.push(msj);
    // this.messages.push(this.mensajesChat.controls.mensaje.value);
    // this.chatService.sendChat(this.mensajesChat.controls.mensaje.value);
    this.chatService.sendChat(msj);
    //this.chatService.sendUserID('idUser');
    this.mensajesChat.reset();
    console.log(this.messageSave);

  }

  getUsers(): any {
    this.chatService.getUser().subscribe(dataUser => {

      this.usersList = dataUser;
    });
  }

  getGroups(): any {
    this.chatService.getGroups().subscribe(dataGroups => {

      const groupListData = dataGroups;
      console.log('id', this.nameUser._id);

      console.log(groupListData);
      // tslint:disable-next-line: prefer-for-of
      for (let index = 0; index < groupListData.length; index++) {
        const find = _.find(groupListData[index].participantes, (o: any) => {
          return o.idUser === this.nameUser._id;
        });
        console.log(find);
        if (find !== undefined) {
          this.groupList.push(groupListData[index]);
        }
      }
      console.log(this.groupList);

    });


  }

  selectUser(userId, userName, userApe): any {
    const datos = {
      nameUser: userName,
      apeUser: userApe,
      idUser: userId
    };
    this.selectedUser = userId;
    this.selectedUserData = datos;
  }

  registerGroup(): any {

    const dataGroup = {
      nombre: this.formCreateGroup.value.nombreGrupo,
      propietario: this.nameUser._id,
      participantes: this.userGroups,

    };
    console.log(dataGroup);
    this.chatService.createGroup(dataGroup).subscribe(dataCreate => {
      console.log(dataCreate);
      alert('creado con exito');

    });

  }
  addUserGroup(): any {
    console.log('hola');
    this.userGroups.push(this.selectedUserData);
    console.log(this.userGroups);
  }
  endConver(): any{

  }
  enterChat(): any{
    
  }


}
